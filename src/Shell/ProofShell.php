<?php
namespace GalleyProof\Shell;

use Cake\Console\Shell;
use Cake\Core\Configure;
use Cake\View\ViewBuilder;
use Cake\I18n\I18n;

class ProofShell extends Shell
{

    public $tasks = [
        'GalleyProof.Render',
        'GalleyProof.Config'
    ];

    public function initialize()
    {
        parent::initialize();
    }

    public function getOptionParser()
    {
        $parser = parent::getOptionParser();
        $parser->addSubcommand('render', [
            'help' => 'Rendering template via definitions.',
            'parser' => $this->Render->getOptionParser()
        ]);
        $parser->addSubcommand('config', [
            'help' => 'Configration util.',
            'parser' => $this->Config->getOptionParser()
        ]);
        $parser->addOption('list', [
            'help' => 'Definition name defined in config/galley_proof.php.',
            'short' => 'l'
        ]);
        return $parser;
    }

    public function main()
    {}
}