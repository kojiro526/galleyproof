<?php
namespace GalleyProof\Shell\Task;

use Cake\Console\Shell;
use Cake\Core\Configure;

class ConfigTask extends Shell
{

    public function getOptionParser()
    {
        $parser = parent::getOptionParser();
        $parser->addOption('list', [
            'help' => 'Listing configurations.',
            'short' => 'l',
            'boolean' => true
        ]);
        return $parser;
    }

    public function main()
    {
        if($this->param('list')){
            $this->_list();
        }
    }
    
    private function _list(){
        $defs = $this->_loadDefinitions();
        foreach($defs as $key => $def){
            echo $key . "\n";
            foreach($def as $attr_name => $attr_val){
                if(is_array($attr_val)){
                    echo '  ' . $attr_name . ': ' . serialize($attr_val) . "\n";
                }else{
                    echo '  ' . $attr_name . ': ' . $attr_val . "\n";
                }
            }
            echo "\n";
        }
    }

    /**
     * 設定ファイルから定義を取得する。
     */
    private function _loadDefinitions()
    {
        $def = Configure::read('GalleyProof.defs');
        return $def;
    }
}