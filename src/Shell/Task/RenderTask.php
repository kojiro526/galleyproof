<?php
namespace GalleyProof\Shell\Task;

use Cake\Console\Shell;
use Cake\Core\Configure;
use Cake\I18n\I18n;
use Cake\View\ViewBuilder;

/**
 * テンプレートをレンダリングするタスク
 *
 * @author sasaki
 */
class RenderTask extends Shell
{

    public function getOptionParser()
    {
        $parser = parent::getOptionParser();
        $parser->addOption('definition', [
            'help' => 'Definition name defined in config/galley_proof.php.',
            'short' => 'd'
        ]);
        return $parser;
    }

    public function main()
    {
        $this->_render();
    }

    /**
     * プレートをレンダリングする。
     *
     * 定義に従ってテンプレートをレンダリングする。
     * 定義は config/galley_proof.php で行う。
     */
    private function _render()
    {
        // 定義ファイルの読み込み
        $defs = $this->_loadDefinitions();
        if (empty($defs)) {
            $this->abort('Definitions is missing.');
        }
        
        $definition = $this->param('definition');
        if (! key_exists($definition, $defs)) {
            $this->abort($definition . ' is not defined in galley_proof.php.');
        }
        
        $def = $defs[$definition];
        
        $locales = null;
        if (key_exists('locales', $def) && is_array($def['locales'])) {
            $locales = $def['locales'];
        } else {
            $locales = [
                'default'
            ];
        }
        
        foreach ($locales as $locale) {
            if ($locale != 'default') {
                I18n::locale($locale);
            }
            
            $builder = new ViewBuilder();
            if (empty($def['layout'])) {
                $layout = null;
            } else {
                $layout = $def['layout'];
            }
            
            if (empty($def['plugin'])) {
                $builder->plugin(null);
            } else {
                $builder->plugin($def['plugin']);
            }
            
            // viewをbuild
            $values = null;
            if (key_exists('values', $def)) {
                if(!is_array($def['values']))
                {
                    $this->abort('"values" must be array.');
                }
                $values = $def['values'];
            }
            $res = $builder->build($values);
            if (empty($def['layout'])) {
                $res->autoLayout = false;
            }
            
            $html = $res->render($def['template'], $layout);
            echo $html;
        }
    }

    /**
     * 設定ファイルから定義を取得する。
     */
    private function _loadDefinitions()
    {
        $def = Configure::read('GalleyProof.defs');
        return $def;
    }
}