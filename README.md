# GalleyProof plugin for CakePHP

## 概要

CakePHP3のテンプレートをレンダリングするプラグインです。

レイアウトファイルやView変数を設定ファイルで指定し、コマンドラインからHTMLをレンダリングすることができます。

## インストール

composerでインストールします。

予め、このリポジトリを`composer.json`に追加しておく必要があります。

```
# composer.json に以下を追記
{
==(略)==
    "repositories": [
        {↲
            "type": "vcs",↲
            "url": "https://bitbucket.org/kojiro526/galleyproof.git"↲
        }
    ],↲
==(略)==
}
``` 

以下のコマンドでインストールします。

```
composer require kojiro526/GalleyProof
```

インストール後、`config/bootstrap_cli.php`に以下を追記します。
~~~
Plugin::load('GalleyProof', ['bootstrap' => true, 'routes' => false]);
~~~

## 使い方

### 設定ファイルを作成

本プラグインをインストールしたCakePHPプロジェクトの`config/`ディレクトリ配下に、`galley_proof.php`を作成します。

上記のファイルに設定を記述します。

例）
~~~
<?php

$config = [];
$config['GalleyProof'] = [];
$config['GalleyProof']['defs'] = [
    'top' => [
        'plugin' => null,
        'layout' => 'default',
        'template' => 'Top/index',
        'values' => [
            'site_title' => 'My Home Page'
        ]
    ]
]
~~~

上記の例では、'top'という名前で定義されたレンダリング設定があります。（レンダリング設定は連想配列で複数設定できます）

上記の設定では、`src/Template/Layout/default.ctp`をレイアウトに指定し、`src/Template/Top/index.ctp`をレンダリングします。

View変数として'site_title'を指定しており、テンプレート内で使用される変数'$site_title'に'My Home Page'が代入されます。

### レンダリング

レンダリングは以下のように行います。

レンダリング結果は標準出力に出力されるため、適当なファイルにリダイレクトします。

~~~
./bin/cake proof render -d top -q > ./webroot/tmp/top_index.html
~~~

ローカルサーバを立ち上げていれば、以下のURLでアクセスできます。

http://localhost:8765/tmp/top_index.html

## リファレンス

レンダリングコマンドの使い方は以下の通りです。

`--definition, -d`オプションで、設定ファイルに記載したレンダリング設定を指定します。

~~~
Usage:
cake galley_proof.proof render [-d] [-h] [-q] [-v]

Options:

--definition, -d  Definition name defined in config/galley_proof.php.
--help, -h        Display this help.
--quiet, -q       Enable quiet output.
~~~